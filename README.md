iCeiRA Diff ROS Control 
====================

A basic work for differential drive platform robots in iCeiRA lab.
This work uses [diff\_driver\_controller](http://wiki.ros.org/diff_drive_controller) from official ROS communicate.

Notice that this work uses serial port to communicate. 
So you have to install LibSerial on your computer first.
Further, we assume that the controller on the robot is Faulhaber controller.
If you want to use other controllers, you have to modify the "read" and "write" 
function in the source code to fit the commands you need.

This package includes:

 - A launch file for basic control settings.  
 - A node for writing commands through serial port. 
 - Beebot urdf model (a differential drive robot in iCeiRA lab) 

You can replace Beebot urdf model with any differential drive robot model you own.

Developed by [Wei Shih](https://bitbucket.org/willie5588912/) at the iCeiRA Lab, National Taiwan University.


### Quick Start 

!! Plug the serial-to-RS232 wire into your computer and chown first !!

Control Settings:
```
roslaunch iceira_diff_ros_control iceira_diff_control.launch
```
Control core node:
```
rosrun iceira_diff_ros_control iceira_diff_ros_control 
```
Now, you can use the gui window to control the robots.
You should see the value change in the terminal.
(They are valuse that will write to the Fauhaber controller, means rpm.)


### Topics 

Quite simple:

 - Subscribe: /cmd\_vel 
 - Publish: /odom, /tf

## Contributors

 - Wei Shih @willie5588912
 - Charly Huang